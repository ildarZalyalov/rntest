import React from 'react';
import {
  StyleSheet,
  View,
  Text,
  StatusBar,
  TextInput,
  Animated,
  Image,
} from 'react-native';

import {PinchGestureHandler, State} from 'react-native-gesture-handler';

export default class TestApp extends React.Component {
  _baseScale = new Animated.Value(1);
  _pinchScale = new Animated.Value(1);
  _scale = Animated.multiply(this._baseScale, this._pinchScale);
  _lastScale = 1;

  constructor(props) {
    super(props);
    this.state = {count: 0, text: ''};
  }

  increaseCount = param => {
    this.setState({count: this.state.count + param});
  };
  changeText = text => {
    this.setState({text});
  };
  _onPinchGestureEvent = Animated.event(
    [{nativeEvent: {scale: this._pinchScale}}],
    {useNativeDriver: true},
  );

  _onPinchHandlerStateChange = event => {
    if (event.nativeEvent.oldState === State.ACTIVE) {
      this._lastScale *= event.nativeEvent.scale;
      this._baseScale.setValue(this._lastScale);
      this._pinchScale.setValue(1);
    }
  };

  render() {
    return (
      <PinchGestureHandler
        onGestureEvent={this._onPinchGestureEvent}
        onHandlerStateChange={this._onPinchHandlerStateChange}>
        <Animated.View
          style={[styles.container, {transform: [{scale: this._scale}]}]}>
          <Text style={styles.paragraph} onPress={() => this.increaseCount(4)}>
            {this.state.text} {this.state.count}
          </Text>
          <TextInput
            style={{
              backgroundColor: 'green',
              height: 40,
              marginHorizontal: 40,
            }}
            onChangeText={this.changeText}
          />
          <View style={{flexDirection: 'column', alignItems: 'center'}}>
            <Image
              source={{
                uri:
                  'https://facebook.github.io/react-native/img/tiny_logo.png',
              }}
              style={{height: 100, width: 100}}
            />
            <Image
              source={{
                uri:
                  'https://facebook.github.io/react-native/img/tiny_logo.png',
              }}
              style={{height: 100, width: 100}}
            />
          </View>
        </Animated.View>
      </PinchGestureHandler>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: StatusBar.currentHeight,
    backgroundColor: '#ecf0f1',
    padding: 8,
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});
